# Quadratic Equation Solver API

This simple Node.js application provides an API endpoint to solve quadratic equations. It uses Express to handle HTTP requests and exposes the endpoint `/quadratic`.

## Usage
1. Clone the repository:

   ```bash
   git clone https://github.com/your-username/quadratic-equation-api.git
   ```

2. Navigate to the project directory:

   ```bash
    cd quadratic-equation-api
    ```

3. Install dependencies:

    ```bash
    npm install express --save
    ```

4. Run the server:

    ```bash
    node app.js
    ```

5. Make a GET request to the /quadratic endpoint with coefficients a, b, and c as query parameters. For example:

    http://127.0.0.1:3000/quadratic?a=1&b=-3&c=2

