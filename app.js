const express = require('express');
const app = express();
const PORT = 3000;

/**
 * Solves a quadratic equation of the form ax^2 + bx + c = 0.
 * @param {number} a - Coefficient of x^2.
 * @param {number} b - Coefficient of x.
 * @param {number} c - Constant term.
 * @returns {(number[] | number | string)} - response
 */
const solve_quadratic = (a, b, c) => {
    if (isNaN(a) || isNaN(b) || isNaN(c)) {
        console.log('Error: Invalid parameters');
        return 'Error: Invalid parameters';
    }

    a = parseFloat(a);
    b = parseFloat(b);
    c = parseFloat(c);

    const discriminant = b * b - 4 * a * c;
    let answer = null;

    if (discriminant > 0) {
        const x1 = (-b + Math.sqrt(discriminant)) / (2 * a);
        const x2 = (-b - Math.sqrt(discriminant)) / (2 * a);
        answer = [x1, x2];
    } else if (discriminant === 0) {
        const x = -b / (2 * a);
        answer = x;
    } else {
        answer = 'No real roots.';
    }
    return answer
};

app.get('/quadratic', (req, res) => {
    const a = req.query.a;
    const b = req.query.b;
    const c = req.query.c;
    answer = solve_quadratic(a, b, c);
    console.log(answer);
    res.send(answer);
});

app.listen(PORT, () => console.log(
    `Listening at http://127.0.0.1:${PORT}`
));
